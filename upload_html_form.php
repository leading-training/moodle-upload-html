<?php

require_once("$CFG->libdir/formslib.php");

class upload_html_form extends moodleform
{

    function definition()
    {

        global $CFG;

        $mform = $this->_form;

        // Select element
        $mform->addElement('select', 'tag', 'Select HTML Tag', array('h1', 'h2'));
        $mform->addElement('checkbox', 'clear', 'Clear Existing Sections');


        // Hidden variables to store current course id and block id
        $mform->addElement('hidden', 'courseid', $this->_customdata['courseid']);
        $mform->setType('courseid', PARAM_INT);
        $mform->addElement('hidden', 'blockid', $this->_customdata['blockid']);
        $mform->setType('blockid', PARAM_INT);

        // Upload file element
        $mform->addElement('filepicker', 'filename', get_string('file'), null, array('accepted_types' => '.html'));
        $mform->setType('MAX_FILE_SIZE', PARAM_INT);
        $mform->addRule('filename', 'Please upload an HTML file', 'required');

        $this->add_action_buttons(false, 'Upload Course');
    }
}
