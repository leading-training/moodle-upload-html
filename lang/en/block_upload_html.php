<?php
$string['pluginname'] = 'Upload HTML block';
$string['upload_html'] = 'Upload HTML';
$string['upload_html:addinstance'] = 'Add a new HTML upload block';
$string['upload_html:myaddinstance'] = 'Add a new HTML upload block to the My Moodle page';
$string['view:edit_page'] = 'Edit Page';
