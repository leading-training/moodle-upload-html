<?php
$plugin->component = 'block_upload_html';  // Recommended since 2.0.2 (MDL-26035). Required since 3.0 (MDL-48494)
$plugin->version = 2020051200;  // YYYYMMDDHH (year, month, day, 24-hr time)
$plugin->requires = 2019111800; // YYYYMMDDHH (This is the release version for Moodle 3.8.3)
