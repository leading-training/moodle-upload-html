<?php

require_once('../../config.php');
require_once('upload_html_form.php');

global $DB, $COURSE;

// Check for all required variables.
$course_id = required_param('courseid', PARAM_INT);

$block_id = required_param('blockid', PARAM_INT);

// Next look for optional variables.
$id = optional_param('id', 0, PARAM_INT);

if (!$course = $DB->get_record('course', array('id' => $course_id))) {
    print_error('Invalid course', 'upload_html_form', $course_id);
}

require_login($course);

$PAGE->set_url('/blocks/upload_html/view.php', array('id' => $id, 'courseid' => $course_id, 'blockid' => $block_id));
$PAGE->set_pagelayout('standard');
$PAGE->set_heading('Upload HTML');

$settingsnode = $PAGE->settingsnav->add('Upload HTML Settings');
$editurl = new moodle_url('/blocks/upload_html/view.php', array('id' => $id, 'courseid' => $course_id, 'blockid' => $block_id));
$editnode = $settingsnode->add('Edit Page', $editurl);
$editnode->make_active();

$htmlForm = new upload_html_form(null, array('courseid' => $course_id, 'blockid' => $block_id));


if ($fromform = $htmlForm->get_data()) {

    $file_content = $htmlForm->get_file_content('filename');
    $form_data = $htmlForm->get_submitted_data();
    $headings_array = array();
    $sections_array = array();

    $form_data->tag == 0 ? $tag = 'h1' : $tag = 'h[12]';
    $form_data->tag == 0 ? $revtag = '1h' : $revtag = '2h';

    // Will retrieve all titles within heading tag. Headings are stored 
    // in matches[1]
    preg_match_all('|<' . $tag . '[^>]*>(.*?)</' . $tag . '>|si', $file_content, $matches);

    for ($i = 0; $i < count($matches[1]); $i++) {
        array_push($headings_array, $matches[1][$i]);
    }

    // Will retrieve all segments between heading tags except the last one. Also 
    // saved in matches[1]
    preg_match_all('|</' . $tag . '>(.*?)<' . $tag . '[^>]*>|si', $file_content, $matches);
    $count = 0;
    //print_object($matches);
    foreach ($matches[1] as $section) {
        array_push($sections_array, $section);
    }

    // Will find final segment between last heading tag and end of html body
    //$tag = strrev($tag);
    $tag = $revtag;
    preg_match_all('|(.*?)>' . $tag . '/<|si', strrev($file_content), $matches);

    array_push($sections_array, strrev($matches[1][0]));

    if (empty($form_data->clear))
        $rows = $DB->count_records('course_sections', ['course' => $course_id]);
    else {
        $rows = 0;
        $DB->delete_records('course_sections', ['course' => $course_id]);
    }
    $objects = array();

    for ($i = 0; $i < count($headings_array); $i++) {
        $object = [
            'course' => $course_id,
            'section' => $i + 1 + $rows,
            'name' => $headings_array[$i],
            'summary' => preg_replace('/^\s+|\r|\s+$/m', '', $sections_array[$i]),
            'summaryformat' => 1,
            'sequence' => '',
            'timemodified' => time()
        ];
        array_push($objects, $object);
    }
    json_encode($objects);
    $DB->insert_records('course_sections', $objects);

    $url = new moodle_url('/course/view.php', array('id' => $course_id));
    redirect($url);
} else {

    //Display the header
    echo $OUTPUT->header();

    // Display the form
    $htmlForm->display();

    // Display the footer
    echo $OUTPUT->footer();
}
