<?php

require_once('upload_html_form.php');

class block_upload_html extends block_base
{
    public function init()
    {
        $this->title = get_string('upload_html', 'block_upload_html');
    }
    // The PHP tag and the curly bracket for the class definition 
    // will only be closed after there is another function added in the next section.
    public function get_content()
    {
        if ($this->content !== null) {
            return $this->content;
        }

        global $CFG, $USER, $COURSE, $OUTPUT;

        $this->content =  new stdClass;

        // The other code.

        $url = new moodle_url('/blocks/upload_html/view.php', array('blockid' => $this->instance->id, 'courseid' => $COURSE->id));
        $this->content->footer = html_writer::link($url, 'Upload HTML');

        return $this->content;
    }

    public function specialization()
    {
        if (isset($this->config)) {
            if (empty($this->config->title)) {
                $this->title = get_string('defaulttitle', 'block_upload_html');
            } else {
                $this->title = $this->config->title;
            }

            if (empty($this->config->text)) {
                $this->config->text = get_string('defaulttext', 'block_upload_html');
            }
        }
    }
}
